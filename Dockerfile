FROM python:2.7.18-slim-stretch

COPY requirements.txt .

# Setup a Working Director Inside Container

WORKDIR /app

# Copy Our Project Folder Inside Image Container's /app Directory

COPY . /app

# Install Panasonic Specific Behave Pakages And Project Specific Required Pakages

RUN apt-get update \
    && python2 -m pip install -r requirements.txt \

    # install chrome

    && apt-get install -y vim \
    && apt-get install -y wget  \    
    && wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb \
    && apt-get install -y ./google-chrome-stable_current_amd64.deb \

    # install chromedriver and put it in path

    && apt-get install -y unzip xvfb libxi6 libgconf-2-4 \
    && apt-get -y install google-chrome-stable \
    && wget https://chromedriver.storage.googleapis.com/2.41/chromedriver_linux64.zip \
    && unzip chromedriver_linux64.zip \
    && mv chromedriver /usr/bin/chromedriver \
    && chmod +x /usr/bin/chromedriver \
